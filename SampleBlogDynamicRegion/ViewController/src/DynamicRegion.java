import oracle.adf.controller.TaskFlowId;

public class DynamicRegion {
    private String taskFlowId = "/WEB-INF/taskFlowSatu.xml#taskFlowSatu";
    private String taskFlowDua = "/WEB-INF/taskFlowDua.xml#taskFlowDua";
    private String currentTF = "taskflowSatu";
    public DynamicRegion() {
    }

    public TaskFlowId getDynamicTaskFlowId() {
        if (this.getCurrentTF().equalsIgnoreCase("taskflowSatu"))
        return TaskFlowId.parse(taskFlowId);
        else 
            return TaskFlowId.parse(taskFlowDua);
    }
    public void setCurrentTF(String currentTF) {
        this.currentTF = currentTF;
    }

    public String getCurrentTF() {
        return currentTF;
    }
}
